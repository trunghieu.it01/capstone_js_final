export class Product {
  constructor(
    id,
    name,
    type,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc
  ) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.price = price;
    this.screen = screen;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.img = img;
    this.desc = desc;
  }
}
