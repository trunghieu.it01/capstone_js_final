const BASE_URL = "https://63576ccd9243cf412f98692f.mockapi.io";

export let getProductList = () => {
  return  axios({
    url: `${BASE_URL}/Admin`,
    method: "GET",
    
  });
};

export let getProductById = (id) => {
  return axios({
    url: `${BASE_URL}/Admin/${id}`,
    method: "GET",
  });
};

export let postProduct = (data) => {
  return axios({
    url: `${BASE_URL}/Admin`,
    method: "POST",
    data: data,
  });
};

export let putProduct = (id, data) => {
  return axios({
    url: `${BASE_URL}/Admin/${id}`,
    method: "PUT",
    data: data,
  });
};

export let deleteProduct = (id) => {
  return axios({
    url: `${BASE_URL}/Admin/${id}`,
    method: "DELETE",
    
  });
};
