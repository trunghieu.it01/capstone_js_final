import { getListProduct, getProductByID } from "./APIServices/services.js";
import {
  addToCartItem,
  filter,
  renderProductsList,
  renderShoppingCart,
} from "./controller/controller.js";
import { Product, CartItem } from "./models/models.js";

const CART = "CART";
let cart = [];

let dataJSON = localStorage.getItem(CART);

if (dataJSON) {
  let dataRaw = JSON.parse(dataJSON);
  cart = dataRaw.map((item) => {
    return new CartItem(item.product, item.quantity);
  });
}

document.getElementById('add-button').addEventListener('click', ()=>{
  batLoading()
  renderShoppingCart(cart)
  closeLoading()
})

export let batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
export let closeLoading = function () {
  document.getElementById("loading").style.display = "none";
};

let saveLocalStorage = () => {
  let dataJSON = JSON.stringify(cart);
  localStorage.setItem(CART, dataJSON);
};

let fecthProductsList = () => {
  batLoading()
  getListProduct()
    .then((item) => {
       renderProductsList(item.data);
      closeLoading()
      renderShoppingCart(cart)
    })
    .catch((err) => {
      console.log("err: ", err);
      closeLoading()
    });
};

fecthProductsList();

let filterList = () => {
  getListProduct()
    .then((item) => {
      filter(item.data);
      closeLoading()
    })
    .catch((err) => {
      console.log("err: ", err);
      closeLoading()
    });
};
window.filterList = filterList;

let add = (itemIndex) => {
  let item = new Product(
    itemIndex.id,
    itemIndex.price,
    itemIndex.name,
    itemIndex.img
  );

  addToCartItem(item, cart);
  return cart;
};

let addItem = (id) => {
  batLoading()
  getProductByID(id)
    .then((res) => {
      let result = add(res.data);
      
      saveLocalStorage();
      closeLoading()
    })
    .catch((err) => {
      console.log("err: ", err);
      closeLoading()
    });
};
window.addItem = addItem;

let increaseItemAmout = (index) => {
  cart[index].quantity++;
  renderShoppingCart(cart);
  saveLocalStorage();
};

window.increaseItemAmout = increaseItemAmout;

let decreaseItemAmount = (index) => {
  if (cart[index].quantity == 1) {
    cart.splice(index, 1);
    renderShoppingCart(cart);
    saveLocalStorage();
  } else {
    cart[index].quantity--;
    renderShoppingCart(cart);
    saveLocalStorage();
  }
};
window.decreaseItemAmount = decreaseItemAmount;

let removeItem = (index) => {
  cart.splice(index, 1);
  renderShoppingCart(cart);
  saveLocalStorage();
};
window.removeItem = removeItem;



let clearCart = () => {
  cart = [];
  renderShoppingCart(cart);
  console.log('cart: ', cart);
  saveLocalStorage();
};

window.clearCart = clearCart;


