import { batLoading,closeLoading } from "../index.js";
import { CartItem, Product } from "../models/models.js";

export let renderProductsList = function (listProducts) {
  let contentHTML = "";
  let contentSelect = "";
  listProducts.forEach(function (item) {
    contentHTML += `<div class="card col-4">
          <div class="card-body ">
            <img
              class="body-img w-100"
              src="${item.img}">
            <div class="moreInfo-overlay">
              <div class="overlay-content">
                <div class="screen">
                  Screen:
                  <span class="screenValue">${item.screen}</span>
                </div>
                <div class="backCamera">
                  Font camera:
                  <span class="backCameraValue">${item.backCamera}</span>
                </div>
                <div class="fontCamera">
                  Back camera:
                  <span class="fontCameraValue">${item.frontCamera}</span>
                </div>
              </div>
            </div>
          </div>
          <div class="card-bot">
          <div class="card-bot-content">
            <div class="phone-name">${item.name}</div>
            <div class="desc">${item.desc}</div>
            <div class="phone-price">${item.price}</div>
            <button onclick="addItem(${item.id})" class='btn btn-primary add'>Add item</button>
          </div>
          </div>
          </div>`;
  });
  document.getElementById("cardContainer").innerHTML = contentHTML;
};
window.renderProductsList = renderProductsList;

export let filter = (itemList) => {
  let selectOption = document.getElementById("filterProduct");
  if (selectOption.value == "") {
    renderProductsList(itemList);
  } else {
    let result = itemList.filter((item) => {
      return item.type === selectOption.value;
    });
    renderProductsList(result);
  }
};

window.filter = filter;

export let addToCartItem = function (itemNeedToAdd, cart) {
  let quantity = 1;
  let product = new CartItem(itemNeedToAdd, quantity);
  if (cart.length == 0) {
    cart.push(product);
  } else {
    for (let i = 0; i < cart.length; i++) {
      if (cart[i].product.id == itemNeedToAdd.id) {
        cart[i].quantity++;
        return;
      } else {
        let yes = cart.find((item) => {
          return item.product.id == itemNeedToAdd.id;
        });
        if (yes == undefined) {
          cart.push(product);
          return;
        }
      }
    }
  }
};
window.addToCartItem = addToCartItem;

let itemTotal = (price, quantity) => {
  return price * quantity;
};

export let renderShoppingCart = (listCartItem) => {
  let contentHTML = "";
  let total = 0;

  listCartItem.forEach((item, index) => {
    total += itemTotal(item.product.price, item.quantity);
    contentHTML += `
    
    <div class="callout d-flex  w-100" >
    <h2 >Binding</h2>
    <button id="closeBtn" class="close-button btn btn-warning" aria-label="Close alert" type="button" data-close>
      <span aria-hidden="true">&times;</span>
    </button>
  
  </div>
     <div class="item d-flex  justify-content-around container mt-3" data-closable>
    <p class="product img">
      <img src="${item.product.img}" alt=""></p>
      <p class="product name">${item.product.name}</p>
      <div class="product">
      <div class=" d-flex justify-content-center">
      <span class="pl-2" onclick="decreaseItemAmount(${index})"> <i class="fa fa-arrow-alt-circle-left "></i></span>
     
      <span class="pl-2" >${item.quantity}</span>
      
      <span class="pl-2" onclick="increaseItemAmout(${index})"><i class="fa fa-arrow-alt-circle-right"></i></span>
      
      </div>
      
      </div>
      <p class="product price">${itemTotal(item.product.price, item.quantity)}</p>
      <div>
      <span class="product" onclick="removeItem(${index})">
      <i class="fa fa-trash"></i></span>
      
      </div>
      </div>
      `;
  });

let pay = document.getElementById('pay')
pay.style.opacity = "1"
let clear = document.getElementById('clear')
clear.style.opacity = "1"
  let totalPrice =document.getElementById("total")
  totalPrice.style.opacity = "1"
  totalPrice.innerHTML = `Total: $${total}`; //in ra total ~ CSS chỗ này bằng id #total
  // button thanh toán + clear + div#total là tag tạo sẵn bên html, không phải từ hàm render.

  let myTab = document.getElementById("shopping-cart");
  
  myTab.style.visibility = "visible";
  myTab.style.transition = "0.5s"
  myTab.style.backgroundColor = "#192841"

  let content = document.getElementById("cart")
  content.style.transition = "0.5s";
  content.style.opacity = "1"
  content.innerHTML = contentHTML;
 
  let closeBtn = document.querySelectorAll("#closeBtn");
if(listCartItem.length == 0){
  myTab.style.visibility = "hidden";
  myTab.style.backgroundColor = "transparent";
  myTab.style.transition = "0.5s";
  pay.style.opacity = "0";
  clear.style.opacity = "0";
  totalPrice.style.opacity= "0";
  totalPrice.style.transition = "0.5s"
}
  if (myTab) {
    
    
    let btnArray = Array.from(closeBtn);
    btnArray.forEach((item) => {
      item.addEventListener("click", () => {
        myTab.style.visibility = "hidden";
        myTab.style.backgroundColor = "transparent";
        myTab.style.transition = "0.5s";
        content.style.transition = "0.5s";
        content.style.opacity = "0";
        totalPrice.style.opacity= "0";
        totalPrice.style.transition = "0.5s"
        pay.style.opacity = "0"
        clear.style.opacity = "0"
     
      });
    });
  }
};
