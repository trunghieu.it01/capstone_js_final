export class Product {
  constructor(id, price, name, img) {
    this.id = id;
    this.price = price;
    this.name = name;
    this.img = img;
  }
}

export class CartItem {
  constructor(product, quantity) {
    this.product = product;
    this.quantity = quantity;
  }
}
