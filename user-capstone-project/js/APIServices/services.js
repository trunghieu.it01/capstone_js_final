import { batLoading } from "../index.js";

const BASE_URL = "https://63576ccd9243cf412f98692f.mockapi.io";

export let getListProduct = () => {
batLoading()
  return axios({
    
    url: `${BASE_URL}/Admin`,
    method: "GET",
    
    
  });
};

export let getProductByID = (id) => {
  batLoading()
  return axios({
    url: `${BASE_URL}/Admin/${id}`,
    method: "GET",
   
  });
};

